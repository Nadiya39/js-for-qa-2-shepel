const {City} = require('./City')
const {Country} = require('./Country')

function compareCitiesByTemperature(city1, city2) {
    return Number.parseInt(city2.weather.temperature) - Number.parseInt(city1.weather.temperature)
}

function getCityPromisies(cityNames){
    return cityNames.map(async (cityName) => {
        let city = new City(cityName)
        await Promise.all([city.setWeather(), city.setForecast()]).catch((error) => {return error.message})
        return city
    })
}

const country1 = new Country('Ukraine')
const cityNames = ['Kyiv', 'Kharkiv', 'Zhitomir', 'Dnipro', 'Lutsk', 'Poltava', 'Sumy', 'Vinnitsa', 'Ternopil', 'Lviv']

Promise.all(getCityPromisies(cityNames)).then(cities => {
    cities.sort(compareCitiesByTemperature)
    cities.forEach(city => country1.addCity(city))
});
