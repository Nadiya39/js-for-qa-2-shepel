class Country {

    constructor(name) {
        this.name = name
        this.cities = []
    }

    addCity(city) {
        this.cities.push(city)
    }

    removeCityByName(cityName) {
        let cityIndex = this.cities.findIndex((city) => city.name === cityName)
        if (cityIndex >= 0 && cityIndex < this.cities.length) {
            this.cities.splice(cityIndex, 1)
        } else {
            throw "Could not find city " + cityName
        }
    }
}

module.exports = {Country}